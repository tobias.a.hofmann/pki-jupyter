# Introduction

This project is a [Juypter Notebook](https://jupyter.org/) showing how to create a PKI using [OpenSSL](https://www.openssl.org/). The configuration for OpenSSL is taken from [my GitLab repositroy PKI](https://gitlab.com/tobias.a.hofmann/pki).

# Content

The notebook will take you through the steps for creating a PKI consisting of a  Root CA and Intermediate CA. In the end, you will be guided on how to use the PKI to request and sign a new server certificate.

![Example Jupyter Notebook](Notebook.png "Example Jupyter Notebook")


# Jupyter Notebook setup

The Docker image must include git and openssl. Both CLI tools are needed to run the notebook. I used Jupyter Lab Notebook in Docker when creating and testing the notebook. The installation guide from Jupyter details [how to run a notebook with Docker](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html#docker). To run a lab version, the environment variable **JUPYTER_ENABLE_LAB=yes** must be provided. From the many [provided Docker images](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html) the minimal-notebook is enough. 

```sh
docker run -p 8888:8888 -e JUPYTER_ENABLE_LAB=yes jupyter/minimal-notebook
```

After initial startup, copy the provided URL with the secure token to be able to log on and change the password. In the below example output, the notebook is available at URL http://127.0.0.1:8888/?token=d98eb0afe4d0d23f9c8c1872cee6c02d87b994e944c853ed

```bash
[I LabApp] Writing notebook server cookie secret to /home/jovyan/.local/share/jupyter/runtime/notebook_cookie_secret
[I LabApp] JupyterLab extension loaded from /opt/conda/lib/python3.7/site-packages/jupyterlab
[I LabApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
[I LabApp] Serving notebooks from local directory: /home/jovyan
[I LabApp] The Jupyter Notebook is running at:
[I LabApp] http://641e47e82b86:8888/?token=d98eb0afe4d0d23f9c8c1872cee6c02d87b994e944c853ed
[I LabApp]  or http://127.0.0.1:8888/?token=d98eb0afe4d0d23f9c8c1872cee6c02d87b994e944c853ed
[I LabApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C LabApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-6-open.html
    Or copy and paste one of these URLs:
        http://641e47e82b86:8888/?token=d98eb0afe4d0d23f9c8c1872cee6c02d87b994e944c853ed
     or http://127.0.0.1:8888/?token=d98eb0afe4d0d23f9c8c1872cee6c02d87b994e944c853ed
```